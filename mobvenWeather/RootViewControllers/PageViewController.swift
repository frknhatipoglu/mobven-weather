//
//  PageViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class PageViewController: UIViewController, UITextFieldDelegate {

    let pageScrollView = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let pageScrollViewFrame = CGRect(x: 0,
                                         y: (self.navigationController?.navigationBar.frame.maxY ?? 0),
                                         width: view.bounds.width,
                                         height: view.bounds.height - (self.navigationController?.navigationBar.frame.maxY ?? 0))
        
        pageScrollView.frame = pageScrollViewFrame
        pageScrollView.showsHorizontalScrollIndicator = false
        pageScrollView.showsVerticalScrollIndicator = false
        pageScrollView.backgroundColor = UIColor.white
        pageScrollView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleBottomMargin, .flexibleTopMargin]
        view.addSubview(pageScrollView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide, object: nil)
    }
    
    var lastKeyboardHeigth: CGFloat = 0 //buna neden gerek var? - kalvye sceure modda iken yuksekligi degisiyor farkli textfieldler arasinda geciste pagescrollview yamulmasin
    
    @objc func keyboardWillShow(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            if lastKeyboardHeigth != keyboardHeight {
                pageScrollView.frame.size = CGSize(width: pageScrollView.bounds.width, height: pageScrollView.bounds.height + lastKeyboardHeigth)
                pageScrollView.frame.size = CGSize(width: pageScrollView.bounds.width, height: pageScrollView.bounds.height - keyboardHeight)
                lastKeyboardHeigth = keyboardHeight
            }
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            pageScrollView.frame.size = CGSize(width: pageScrollView.bounds.width, height: pageScrollView.bounds.height + keyboardHeight)
            lastKeyboardHeigth = -1
            
        }
    }
    
    
    //klavyenin return tusunun kavyeyi kapatmasi icin
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

}
