//
//  BaseViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class BaseViewController: PageViewController {

    var activityIndicator: ActivityIndicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    func setNavigationBarTitle (text: String) {
        self.navigationItem.title = text
    }
    
    func setActivityIndicator () {
        activityIndicator = ActivityIndicator(frame: pageScrollView.frame)
        activityIndicator?.layer.zPosition = 1
        self.pageScrollView.addSubview(activityIndicator!)
    }
    
    func createAlertView (title: String = "", msg: String, closeButton: String = "Close", parent: UIViewController) {
        
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: closeButton, style: .default, handler: nil))
        parent.present(alert, animated: true, completion: nil)
        
    }

}
