//
//  LoadingViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit


class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        setLoadingView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        setKeyWindowToHomeViewController()
    }
    
    let logoImageView = UIImageView()
    let activityIndicator = UIActivityIndicatorView()
    
    func setLoadingView () {
        
        logoImageView.frame = CGRect(x: view.bounds.midX - 125, y: self.view.bounds.midY - 60, width: 250, height: 120)
        logoImageView.image = UIImage(named: "company-logo")
        logoImageView.contentMode = .scaleAspectFit
        logoImageView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin,.flexibleBottomMargin, .flexibleTopMargin]
        view.addSubview(logoImageView)
        
        activityIndicator.activityIndicatorViewStyle = .whiteLarge
        activityIndicator.frame = CGRect(x: view.bounds.midX - 15, y: logoImageView.frame.maxY + 20, width: 30, height: 30)
        activityIndicator.color = UIColor.white
        activityIndicator.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin,.flexibleBottomMargin, .flexibleTopMargin]
        activityIndicator.startAnimating()
        view.addSubview(activityIndicator)
    }
    
    func setKeyWindowToHomeViewController () {
    
        let rootViewController = self.storyboard?.instantiateViewController(withIdentifier: "MainNavigationController")
        
        let keyWindow = UIApplication.shared.keyWindow
        keyWindow?.rootViewController = rootViewController
        keyWindow?.makeKeyAndVisible()
    
    }

}

