//
//  WeatherDetailTableViewCell.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit
import Foundation

class WeatherDetailTableViewCell: UITableViewCell {
    
    var titleLabel = UILabel()
    var subTitleLabel = UILabel()
    var separatorView = UIView()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        titleLabel.textColor = UIColor.black
        titleLabel.font = UIFont.systemFont(ofSize: 16)
        titleLabel.numberOfLines = 1
        titleLabel.adjustsFontSizeToFitWidth = true
        contentView.addSubview(titleLabel)
        
        subTitleLabel.textColor = UIColor.darkGray
        subTitleLabel.font = UIFont.systemFont(ofSize: 12)
        subTitleLabel.numberOfLines = 1
        subTitleLabel.adjustsFontSizeToFitWidth = true
        contentView.addSubview(subTitleLabel)
    
        separatorView.backgroundColor = UIColor.black
        contentView.addSubview(separatorView)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        titleLabel.frame = CGRect(x: 20, y: 0, width: (bounds.width - 40) / 2, height: bounds.height)
        subTitleLabel.frame = CGRect(x: frame.midX + 10, y: 0, width: bounds.width / 2, height: bounds.height)
        separatorView.frame = CGRect(x: frame.midX, y: 0, width: 1, height: bounds.height)
    }
    
}
