//
//  WeatherDetailTableView.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class WeatherDetailTableView  : UITableView, UITableViewDelegate, UITableViewDataSource {

    var data = [keyValueStruct]()

    var cellHeight: CGFloat = 50
    var cellStyleForEditing: UITableViewCellEditingStyle = .none
    
    init(frame: CGRect, tableViewData: [keyValueStruct]) {
        
        self.data = tableViewData
    
        super.init(frame: frame, style: .plain)
        self.dataSource = self
        self.delegate = self
        self.backgroundColor = UIColor.clear
        self.register(WeatherDetailTableViewCell.self, forCellReuseIdentifier: "tableViewCell")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCell", for: indexPath) as! WeatherDetailTableViewCell
        let cellData = data[indexPath.item]
        cell.titleLabel.text = cellData.key
        cell.subTitleLabel.text = cellData.value
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.deselectRow(at: indexPath, animated: true)
    }
}
