//
//  WeatherListCollectionView.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//


import Foundation
import UIKit
import SDWebImage

protocol WeatherListCollectionViewDelegate {
    func weatherListCollectionViewSelectedItemData (weatherData: weatherDataStruct)
}

class WeatherListCollectionView : UICollectionView, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
   
    var userDelegate : WeatherListCollectionViewDelegate?
    var weatherDatas = [weatherDataStruct]()
    var addCityNamesToCell = false
    
    init (frame: CGRect, delegate: WeatherListCollectionViewDelegate) {
        
        super.init(frame: frame, collectionViewLayout: UICollectionViewFlowLayout())
        
        self.userDelegate = delegate
        self.collectionViewLayout = setLayout()
        self.dataSource = self
        self.delegate = self
        self.register(WeatherListCollectionViewWeatherCell.self, forCellWithReuseIdentifier: "weatherCell")
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.backgroundColor = UIColor.clear
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setLayout() -> UICollectionViewFlowLayout {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0)
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        layout.estimatedItemSize = CGSize(width: self.bounds.width, height: 100)
        return layout
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.bounds.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return weatherDatas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "weatherCell", for: indexPath) as! WeatherListCollectionViewWeatherCell
        let cellData = weatherDatas[indexPath.item]
        
        cell.weatherIconImageView.sd_setImage(with: URL(string: cellData.detail.iconUrl))
        cell.weatherInfoLabel.text = "\(addCityNamesToCell ? (cellData.city + " ") : "")\(cellData.detail.main) - \(cellData.detail.description)"
        cell.mainInfoLabel.text = "current: \(cellData.mainInfo.temp) min: \(cellData.mainInfo.minTemp) max: \(cellData.mainInfo.maxTemp) pressure: \(cellData.mainInfo.pressure) sea level: \(cellData.mainInfo.seaLevel)"
        cell.windInfoLabel.text = "wind speed: \(cellData.windSpeed) wind degree: \(cellData.windDeg)"

        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        userDelegate?.weatherListCollectionViewSelectedItemData(weatherData: weatherDatas[indexPath.item])
    }
}
