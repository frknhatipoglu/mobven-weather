//
//  WeatherListCollectionViewWeatherCell.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation
import UIKit

class WeatherListCollectionViewWeatherCell : UICollectionViewCell {
    
    let weatherIconImageView = UIImageView()
    let weatherInfoLabel = UILabel()
    let mainInfoLabel = UILabel()
    let windInfoLabel = UILabel()
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

        weatherIconImageView.frame = CGRect(x: 0, y: 0, width: self.bounds.height, height: self.bounds.height)
        weatherIconImageView.autoresizingMask = [.flexibleRightMargin]
        weatherIconImageView.contentMode = .scaleAspectFit
        contentView.addSubview(weatherIconImageView)
        
        weatherInfoLabel.frame = CGRect(x: weatherIconImageView.frame.maxX, y: 0, width: self.bounds.width - weatherIconImageView.frame.maxX, height: self.bounds.height / 3)
        weatherInfoLabel.autoresizingMask = [.flexibleWidth]
        contentView.addSubview(weatherInfoLabel)
        
        mainInfoLabel.frame = CGRect(x: weatherIconImageView.frame.maxX, y: weatherInfoLabel.frame.maxY, width: weatherInfoLabel.bounds.width, height: self.bounds.height / 3)
        mainInfoLabel.autoresizingMask = [.flexibleWidth]
        mainInfoLabel.textColor = UIColor.darkGray
        contentView.addSubview(mainInfoLabel)
        
        windInfoLabel.frame = CGRect(x: weatherIconImageView.frame.maxX, y: mainInfoLabel.frame.maxY, width: weatherInfoLabel.bounds.width, height: self.bounds.height / 3)
        windInfoLabel.autoresizingMask = [.flexibleWidth]
        windInfoLabel.textColor = UIColor.lightGray
        contentView.addSubview(windInfoLabel)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }

    
}
