//
//  ActivityIndicator.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation
import UIKit

class ActivityIndicator: UIActivityIndicatorView {
    
    override init(frame: CGRect) {
        
        let newFrame = CGRect(x: frame.midX - 15, y: frame.midY - 15, width: 30, height: 30)
        
        super.init(frame: newFrame)
        
        self.isUserInteractionEnabled = false
        self.color = UIColor.black
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
