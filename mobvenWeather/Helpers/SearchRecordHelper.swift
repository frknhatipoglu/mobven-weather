//
//  SearchRecordHelper.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation

let searchRecordHelper = SearchRecordHelper()

class SearchRecordHelper {

    private var searchKeys = [String]()
    
    init() {
        
        if let searchKeysCheck = UserDefaults.standard.array(forKey: "userSearchRecords") as? [String] {
            searchKeys = searchKeysCheck
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(SearchRecordHelper.appDidEnterBackground(_:)), name: Notification.Name.UIApplicationDidEnterBackground, object: nil)
    }
    
    func getSearchKeys () -> [String] {
        return searchKeys
    }
    
    func addSearchKey (searchKey: String) {
//        to prevent record repetition if array contaions search key remove that index and add end of the array
        if let keyIndex = searchKeys.index(of: searchKey) {
            searchKeys.remove(at: keyIndex)
        }
        
        searchKeys.append(searchKey)
        
    }
    
    @objc private func appDidEnterBackground (_ sender: NSNotification) {
        saveSearchKeys()
    }
    
    private func saveSearchKeys () {
        print("saveSearchKeys")
        UserDefaults.standard.set(searchKeys, forKey: "userSearchRecords")
    }
}
