//
//  Extensions.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    func getText() -> String {
        if let text = self.text {
            return text
        }
        return ""
    }
}
