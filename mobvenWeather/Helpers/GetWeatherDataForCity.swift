//
//  GetWeatherDataForCity.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation

class GetWeatherDataForCity {
    
    var completionHandler: ((weatherDataStruct, String, Int) -> ())!
    var callIndex: Int!
    
    init(cityName: String, callIndex: Int, completionHandler: @escaping (weatherDataStruct, String, Int)->()) {
        
        self.callIndex = callIndex
        self.completionHandler = completionHandler
        
        getWeatherData(cityName: cityName)
        
    }
    
    func getWeatherData (cityName: String) {
        _ = ServiceFunctions().getWeatherData(city: cityName, completionHandler: { (responseData) in
            
            if let parsedData = DataParser().parseWeatherData(responseData: responseData).first {
                self.completionHandler(parsedData, cityName, self.callIndex)
            } else {
                var weatherData = weatherDataStruct()
                weatherData.detail.main = "Data is not avaliable for \(cityName)"
                self.completionHandler(weatherData, cityName, self.callIndex)
            }
        })
    }
}
