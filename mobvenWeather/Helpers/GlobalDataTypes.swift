//
//  GlobalDataTypes.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation

enum serviceTypeEnum {
    case getWeatherData
}


struct serviceInitDataStruct {
    var requestIndex: Int!
    var searchText: String!
}

struct weatherDataStruct {
    var city = ""
    var mainInfo = weatherMainInfoDataStruct()
    var detail = weatherDetailDataStruct()
    var cloud: Int = 0
    var rain: Float = 0.0
    var windDeg: Float = 0.0
    var windSpeed: Float = 0.0
}

struct weatherMainInfoDataStruct {
    var grndLevel: Float = 0.0
    var minTemp: Float = 0.0
    var maxTemp: Float = 0.0
    var temp: Float = 0.0
    var seaLevel: Float = 0.0
    var pressure: Float = 0.0
}

struct weatherDetailDataStruct {
    var main: String = ""
    var description: String = ""
    var id: Int = -1
    var iconUrl: String = ""
}

struct keyValueStruct {
    var key: String!
    var value: String!
}
