//
//  DataParser.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation
import SwiftyJSON

class DataParser {
    
    init() {
        
    }
    
    func parseWeatherData (responseData: Data) -> [weatherDataStruct] {
        
        var weatherDatas = [weatherDataStruct]()
        let responseJson = JSON(responseData)
        
        let cityName = responseJson["city"]["name"].stringValue
        
        for item in responseJson["list"] {
            
            var weather = weatherDataStruct()
            weather.mainInfo    = parseMainInfoData(mainInfoJson: item.1["main"])
            weather.detail      = parseWeatherDetail(weatherDetailJson: item.1["weather"])
            weather.cloud       = item.1["clouds"]["all"].intValue
            weather.rain        = item.1["rain"]["3h"].floatValue
            weather.windDeg     = item.1["wind"]["deg"].floatValue
            weather.windSpeed   = item.1["wind"]["speed"].floatValue
            weather.city        = cityName
            weatherDatas.append(weather)
        }
        
        return weatherDatas
    }
    
    private func parseMainInfoData (mainInfoJson: JSON) -> weatherMainInfoDataStruct {
        var weatherMainInfo = weatherMainInfoDataStruct()
        weatherMainInfo.grndLevel   = mainInfoJson["grnd_level"].floatValue
        weatherMainInfo.minTemp     = mainInfoJson["temp_min"].floatValue
        weatherMainInfo.maxTemp     = mainInfoJson["temp_max"].floatValue
        weatherMainInfo.temp        = mainInfoJson["temp"].floatValue
        weatherMainInfo.pressure    = mainInfoJson["pressure"].floatValue
        weatherMainInfo.seaLevel    = mainInfoJson["sea_level"].floatValue
        
        return weatherMainInfo
    }
    
    private func parseWeatherDetail (weatherDetailJson: JSON) -> weatherDetailDataStruct {
        let weatherDetail = weatherDetailDataStruct(main: weatherDetailJson[0]["main"].stringValue,
                                                    description: weatherDetailJson[0]["description"].stringValue,
                                                    id: weatherDetailJson[0]["id"].intValue,
                                                    iconUrl: "http://openweathermap.org/img/w/" + weatherDetailJson[0]["icon"].stringValue + ".png")
        return weatherDetail
    }
}
