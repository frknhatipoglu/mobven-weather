//
//  HomeViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController, ServiceFunctionsDelegate, WeatherListCollectionViewDelegate {

    let searchTextField = UITextField()
    let searchButton = UIButton(type: .system)
    var weatherListCollectionView: WeatherListCollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setNavigationBarTitle(text: "Mobven")
        setPreviousSearchesNavigationButton()
        setComponents()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setComponents () {
        
        setSearchTextField()
        setSearchButton()
        setWeatherCollectionView()
        setActivityIndicator()
    }
    
    func setPreviousSearchesNavigationButton () {
        let previousSearchesButton = UIBarButtonItem()
        previousSearchesButton.image = UIImage(named: "history")
        previousSearchesButton.tintColor = UIColor.black
        previousSearchesButton.target = self
        previousSearchesButton.action = #selector(HomeViewController.previousSearchesButtonPressed(_:))
        navigationItem.setRightBarButton(previousSearchesButton, animated: true)
    }
    
    @objc func previousSearchesButtonPressed (_ sender: UIBarButtonItem) {
        let previousSearchesView = self.storyboard?.instantiateViewController(withIdentifier: "PreviousSearchesView") as! PreviousSearchesViewController
        self.navigationController?.pushViewController(previousSearchesView, animated: true)
    }
    
    
    
    func setSearchTextField () {
        
        searchTextField.frame = CGRect(x: pageScrollView.bounds.midX - 150, y: 0, width: 300, height: 50)
        searchTextField.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        searchTextField.placeholder = "search"
        searchTextField.backgroundColor = UIColor.lightGray
        searchTextField.delegate = self
        pageScrollView.addSubview(searchTextField)
    }
    
    
    func setSearchButton () {
        
        searchButton.frame = CGRect(x: searchTextField.frame.minX, y: searchTextField.frame.maxY, width: searchTextField.bounds.width, height: 50)
        searchButton.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleBottomMargin]
        searchButton.setTitle("Search", for: .normal)
        searchButton.addTarget(self, action: #selector(HomeViewController.searchButtonPressed(_:)), for: .touchUpInside)
        pageScrollView.addSubview(searchButton)
    }

    @objc func searchButtonPressed (_ sender: UIButton) {
        processCityText(cityText: searchTextField.getText())
    }
    
    
    func setWeatherCollectionView () {
        // 100: searchTextField.frame.maxY + 50 + searchButton height
        let weatherListFrame = CGRect(x: 0, y: searchButton.frame.maxY, width: pageScrollView.bounds.width, height: pageScrollView.bounds.height - searchButton.frame.maxY)
        weatherListCollectionView = WeatherListCollectionView(frame: weatherListFrame, delegate: self)
        weatherListCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        pageScrollView.addSubview(weatherListCollectionView)
    }
    
    func weatherListCollectionViewSelectedItemData (weatherData: weatherDataStruct) {
        let previousViewController = self.storyboard?.instantiateViewController(withIdentifier: "WeatherDetailView") as! WeatherDetailViewController
        previousViewController.weatherData = weatherData
        self.navigationController?.pushViewController(previousViewController, animated: true)
    }
    
    func processCityText (cityText: String) {
        
        if cityText == "" {
            print("search box is empty")
            createAlertView(msg: "Please, type the city that you want to learn weather", parent: self)
            return
        }
        callWeatherService(cityText: cityText)
    }
    
    func callWeatherService (cityText: String) {
        
        weatherListCollectionView.weatherDatas.removeAll()
        weatherListCollectionView.reloadData()
        activityIndicator?.startAnimating()
        
        var serviceData = serviceInitDataStruct()
        serviceData.searchText = cityText
        _ = ServiceFunctions(type: .getWeatherData, delegate: self, data: serviceData)
    }
    
    func serviceResponseData(responseServiceType: serviceTypeEnum, responseData: Data, serviceInitData : serviceInitDataStruct) {
        
        switch responseServiceType {
        case .getWeatherData:
            processWeatherData(responseData: responseData, searchKey: serviceInitData.searchText)
        default:
            break
        }
        
    }
    
    func processWeatherData (responseData: Data, searchKey: String) {
        
        activityIndicator?.stopAnimating()
        
        let parsedData = DataParser().parseWeatherData(responseData: responseData)
        
        if parsedData.count == 0 {
            createAlertView(msg: "No results found", parent: self)
            return
        }
        // add search key after checking results
        searchRecordHelper.addSearchKey(searchKey: searchKey)
        weatherListCollectionView.weatherDatas = parsedData
        weatherListCollectionView.reloadData()
    }
}
