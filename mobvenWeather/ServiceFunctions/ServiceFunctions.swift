//
//  ServiceFunctions.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

protocol ServiceFunctionsDelegate {
    func serviceResponseData(responseServiceType: serviceTypeEnum, responseData: Data, serviceInitData : serviceInitDataStruct)
}

class ServiceFunctions {
    
    
    var userDelegate : ServiceFunctionsDelegate?
    var serviceType : serviceTypeEnum!
    var serviceData : serviceInitDataStruct!
    
//    an empty init to provide access methods (added in future) directly
    init() {
        
    }
    
    init(type: serviceTypeEnum, delegate : ServiceFunctionsDelegate , data : serviceInitDataStruct){
        
        userDelegate = delegate
        self.serviceType = type
        self.serviceData = data
        
        switch type {
            
        case .getWeatherData:
            getWeatherData(city: serviceData.searchText)
        default:
            print("service type hata : \(type)")
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func getWeatherData (city: String, completionHandler: @escaping (_ responseData: Data) -> () = { _ in }) {
        
        let appId = "8827fbf408dc7e1418f3c1e84596334c"
        let url = "http://api.openweathermap.org/data/2.5/forecast"
        
        let paramDict = ["q": city,
                         "units": "metric",
                         "APPID": appId]
        
        Alamofire.request(url, method: .get, parameters: paramDict, encoding: URLEncoding.default, headers: nil).response { (response) in
            if let data = response.data as Data? {
                self.userDelegate?.serviceResponseData(responseServiceType: self.serviceType, responseData: data, serviceInitData: self.serviceData)
                completionHandler(data)
            }
        }
    }

    
}
