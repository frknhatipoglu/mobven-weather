//
//  PreviousSearchesViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class PreviousSearchesViewController: BaseViewController, WeatherListCollectionViewDelegate {
  
    var weatherListCollectionView: WeatherListCollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setComponents()
        getDatas()
        setNavigationBarTitle(text: "History")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setComponents () {
        setWeatherCollectionView()
    }
    
    func setWeatherCollectionView () {
        // 100: searchTextField.frame.maxY + 50 + searchButton height
        weatherListCollectionView = WeatherListCollectionView(frame: pageScrollView.bounds, delegate: self)
        weatherListCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        weatherListCollectionView.addCityNamesToCell = true
        pageScrollView.addSubview(weatherListCollectionView)
    }
    
    func weatherListCollectionViewSelectedItemData(weatherData: weatherDataStruct) {
        
//        default detail id is -1. if detail id is -1 that data didn't load, return.
        if weatherData.detail.id == -1 {
            createAlertView(msg: "Please wait while data is loading", parent: self)
            return
        }
        
        let previousViewController = self.storyboard?.instantiateViewController(withIdentifier: "WeatherDetailView") as! WeatherDetailViewController
        previousViewController.weatherData = weatherData
        self.navigationController?.pushViewController(previousViewController, animated: true)
    }
    
    func getDatas () {
        setInitialDataForWeatherListCollectionView()
        getWeatherDataForCities()
    }
    
    func setInitialDataForWeatherListCollectionView () {
        var initialWeatherDatas = [weatherDataStruct]()
        for city in searchRecordHelper.getSearchKeys().reversed() {
            var initWeatherData = weatherDataStruct()
            initWeatherData.detail.main = "Waiting for city of \(city) weather data"
            
            initialWeatherDatas.append(initWeatherData)
        }
        
        weatherListCollectionView.weatherDatas = initialWeatherDatas
        weatherListCollectionView.reloadData()
    }
    
    var callIndex = 0
    func getWeatherDataForCities () {
        
        for city in searchRecordHelper.getSearchKeys().reversed() {
            _ = GetWeatherDataForCity(cityName: city, callIndex: callIndex, completionHandler: { (weatherData, city, reqIndex)  in
                DispatchQueue.main.async {
                    self.setWeatherDataOnWeatherCollectionView(weatherData: weatherData, city: city, reqIndex: reqIndex)
                }
            })
            callIndex += 1
        }
    }
    
    
    func setWeatherDataOnWeatherCollectionView (weatherData: weatherDataStruct, city: String, reqIndex: Int) {
        
        if let cell = weatherListCollectionView.cellForItem(at: IndexPath(item: reqIndex, section: 0)) as? WeatherListCollectionViewWeatherCell {
            cell.weatherIconImageView.sd_setImage(with: URL(string: weatherData.detail.iconUrl))
            cell.weatherInfoLabel.text = "\(weatherData.city) \(weatherData.detail.main) - \(weatherData.detail.description)"
            cell.mainInfoLabel.text = "current: \(weatherData.mainInfo.temp) min: \(weatherData.mainInfo.minTemp) max: \(weatherData.mainInfo.maxTemp) pressure: \(weatherData.mainInfo.pressure) sea level: \(weatherData.mainInfo.seaLevel)"
            cell.windInfoLabel.text = "wind speed: \(weatherData.windSpeed) wind degree: \(weatherData.windDeg)"
        }
        if reqIndex < weatherListCollectionView.weatherDatas.count {
            weatherListCollectionView.weatherDatas[reqIndex] = weatherData
        }
    }
    

}
