//
//  WeatherDetailViewController.swift
//  mobvenWeather
//
//  Created by Erstream on 1.07.2018.
//  Copyright © 2018 frknhatipoglu. All rights reserved.
//

import UIKit

class WeatherDetailViewController: BaseViewController {

    var weatherData: weatherDataStruct!   // coming from parent
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setComponents()
        setNavigationBarTitle(text: weatherData.city)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setComponents () {
        setImageView()
        setWeatherDetailsTableView()
    }
    
    func setImageView () {
        let iconImageView = UIImageView()
        iconImageView.frame = CGRect(x: 0, y: 0, width: pageScrollView.bounds.midX, height: pageScrollView.bounds.height)
        iconImageView.autoresizingMask = [.flexibleWidth, .flexibleHeight, .flexibleRightMargin]
        iconImageView.contentMode = .scaleAspectFit
        iconImageView.sd_setImage(with: URL(string: weatherData.detail.iconUrl))
        pageScrollView.addSubview(iconImageView)
    }
    
    func setDetailTextView () {
        let detailTextView = UITextView()
        detailTextView.frame = CGRect(x: pageScrollView.frame.midX, y: 0, width: pageScrollView.bounds.width / 2, height: pageScrollView.bounds.height)
        detailTextView.autoresizingMask = [.flexibleLeftMargin, .flexibleHeight, .flexibleWidth]
        
        var detailText = ""
        
        let mirror = Mirror(reflecting: weatherData)
        for child in mirror.children {
            if let label = child.label {
                detailText += "\(label): \(child.value)\n"
            }
        }
        
        detailTextView.text = detailText
        detailTextView.isEditable = false
        detailTextView.isSelectable = false
        pageScrollView.addSubview(detailTextView)
    }
    
    func setWeatherDetailsTableView () {
        let tableViewFrame = CGRect(x: pageScrollView.frame.midX, y: 0, width: pageScrollView.bounds.width / 2, height: pageScrollView.bounds.height)
        var weatherDetailData = [keyValueStruct]()
        

        weatherDetailData.append(keyValueStruct(key: "City:", value: weatherData.city))
        weatherDetailData.append(keyValueStruct(key: "Summary:", value: weatherData.detail.main))
        weatherDetailData.append(keyValueStruct(key: "Cloud:", value: String(weatherData.cloud)))
        weatherDetailData.append(keyValueStruct(key: "GrndLevel:", value: String(weatherData.mainInfo.grndLevel)))
        weatherDetailData.append(keyValueStruct(key: "Current:", value: String(weatherData.mainInfo.temp)))
        weatherDetailData.append(keyValueStruct(key: "Max:", value: String(weatherData.mainInfo.maxTemp)))
        weatherDetailData.append(keyValueStruct(key: "Min:", value: String(weatherData.mainInfo.minTemp)))
        weatherDetailData.append(keyValueStruct(key: "Pressure:", value: String(weatherData.mainInfo.pressure)))
        weatherDetailData.append(keyValueStruct(key: "Sea Level:", value: String(weatherData.mainInfo.seaLevel)))
        weatherDetailData.append(keyValueStruct(key: "Rain:", value: String(weatherData.rain)))
        weatherDetailData.append(keyValueStruct(key: "Wind Deg:", value: String(weatherData.windDeg)))
        weatherDetailData.append(keyValueStruct(key: "Wind Speed:", value: String(weatherData.windSpeed)))
        
        let weatherDetailTableView = WeatherDetailTableView(frame: tableViewFrame, tableViewData: weatherDetailData)
        weatherDetailTableView.autoresizingMask = [.flexibleLeftMargin, .flexibleHeight, .flexibleWidth]
        pageScrollView.addSubview(weatherDetailTableView)
    }
}
